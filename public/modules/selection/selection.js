var $ = require('jquery')
var Populate = require('./populate')

var Selection = {
  battlefield: Populate.selectedBattlefield,
  enemy: Populate.selectedDifficulty,
  difficulty: Populate.selectedEnemy
}

module.exports = Selection
