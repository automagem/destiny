var BattlefieldList = require('../../data/battlefield.json')
var $ = require('jquery')

var Populate = {
  selectedBattlefield: { code: '05178' },
  selectedDifficulty: 'Normal',
  selectedEnemy: 'Luke',
  tableBattlefields: function () {
    var battlefields = BattlefieldList
    var index = 1
    $.each(battlefields, function (i, battlefield) {
      var number = '<th scope="row">' + index + '</th>'
      var name = '<td>' + battlefield.name + ' (' + battlefield.subtitle + ')'
      var affiliation = '</td><td>' + battlefield.affiliation_name
      var rarity = '</td><td>' + battlefield.rarity_name
      var set = '</td><td>' + battlefield.set_name + ' #' + battlefield.position

      var rowText = '<tr>' + number + name + affiliation + rarity + set + '</td></tr>'
      $('#tableBattlefields').append(rowText)
      index++
    })
    // handle click event on table to select battlefield
    $(function () {
      $('tr').click(function () {
        $('tr').removeClass('table-success')
        $(this).addClass('table-success')
        var index = this.rowIndex - 1
        Populate.selectedBattlefield = battlefields[index]
      })
    })
  },
  difficulty: function () {
    var difficultyList = '<ul class="list-group">' +
      '<li class="list-group-item">Starter</li>' +
      '<li class="list-group-item">Facil</li>' +
      '<li class="list-group-item active" aria-current="true">Normal</li>' +
      '<li class="list-group-item">Dificil</li>' +
      '</ul>'
    $('#difficultyList').append(difficultyList)

    // handle click event
    $(function () {
      $('li').click(function () {
        $('li').removeClass('active')
        $(this).addClass('active')
        Populate.selectedDifficulty = this.textContent
        console.log(this.textContent)
      })
    })
  },
  enemies: function () {
    var enemies = [
      { name: 'Dath Vader', code: 'vader', text: 'Sith Lord' },
      { name: 'Boba Fett', code: 'boba', text: 'Infamous Bounty Hunter' },
      { name: 'General Grievous', code: 'grievous', text: 'Commanding Legions' },
      { name: 'Luke Skywalker', code: 'luke', text: 'Jedi Knight' }
    ]
    var folder = './data/enemies/'

    var cardGroupHTML = '<div class="row card-group" id="enemyCards">'
    $('#enemyList').append(cardGroupHTML)
    $.each(enemies, function (i, enemy) {
      var enemyCardHTML = '<div class="card" style="width: 18rem;">' +
        '<img src="' + folder + enemy.code + '/' + enemy.code + '.jpg" class="card-img-top" alt="' + enemy.name + '">' +
        '<div class="card-body">' +
        // '<h5 class="card-title">' + enemy.name + '</h5>' +
        '<p class="card-text">' + enemy.text + '</p>' +
        '<button class="btn btn-primary enemy" name="' + enemy.code + '">Empezar</button>' +
        '</div>' +
        '</div>'
      $('#enemyCards').append(enemyCardHTML)
    })
    // handle click event
    $(function () {
      $('.enemy').click(function () {
        Populate.selectedEnemy = this.name
        // var enemyCode = this.name
        // this.selectedEnemy = enemyCode
        var url = 'ia.html?battlefield=' + Populate.selectedBattlefield.code +
          '&difficulty=' + Populate.selectedDifficulty + '&enemy=' + Populate.selectedEnemy

        window.location.href = url
      })
    })
  }
}

module.exports = Populate
