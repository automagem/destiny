# Star Wars Destiny Encounters


## Getting started

Add Node Modules:
```
cd public
npm install
```

Run scripts:
```
npm run build #build bundle.js
npm run watch #watchify
```

Star a beefy server on http://127.0.0.1:9966
```
npm run start
```

## Description
Star Wars Destiny Encounters is a IA created by [AzureDeath], you can find more information in the [Encounters] BGG thread.

## Usage
Work in progress.

## Support
Check [Issues] page.

## Roadmap
This is a pet project created for me to use the **Star Wars Destiny Encounters** enemies decks online without having to have them printed.

## Authors and acknowledgment
This project is a web implementation of **Encounters: A Star Wars Destiny Solo / Co-op Variant** created by [AzureDeath]

I got the battlefield Json card list from the [SWDestinyDB API]:

https://swdestinydb.com/api/public/find?q=t%3Abattlefield

## License
GNU GENERAL PUBLIC LICENSE

## Project status
Just started


[AzureDeath]: https://boardgamegeek.com/filepage/232464/encounters-star-wars-destiny-solo-co-op-variant
[Encounters]: https://boardgamegeek.com/thread/2774639/encounters-star-wars-destiny-solo-co-op-variant
[Issues]: https://gitlab.com/automagem/destiny/-/issues
[SWDestinyDB API]: https://swdestinydb.com/api/
